#!/bin/bash -ex

# Install curl and jq for API requests and JSON parsing
apt-get update && apt-get install -y curl unzip

curl --location https://gitlab.com/Linaro/lkft/mirrors/android/auto-merge-lts-updates/-/jobs/artifacts/main/download?job=${CI_JOB_NAME} --output artifacts.zip 

unzip -o artifacts.zip
